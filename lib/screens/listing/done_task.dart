import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:todo_firebase/screens/navigation.dart';

import '../dashboard/home_page.dart';

class Done extends StatefulWidget {
  @override
  _DoneState createState() => _DoneState();
}

class _DoneState extends State<Done> {
  //String uid = '';

  @override
  void initState() {
    // TODO: implement initState

    // getuid();

    super.initState();
  }

  // getuid() async {
  //   FirebaseAuth auth = FirebaseAuth.instance;
  //   final User user = await auth.currentUser;
  //   setState(() {
  //     uid = user.uid;
  //     print(uid);
  //   });
  // }


  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context) => LoadDataFromFireStore())),
      child: Scaffold(
        // appBar: AppBar(
        //   centerTitle: true,
        //   title: Text("Done"),
        //   actions: <Widget>[
        //     IconButton(
        //         icon: Icon(Icons.logout),
        //         onPressed: () async {
        //           await FirebaseAuth.instance.signOut();
        //         })
        //   ],
        //),
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          // color: Colors.black,
          child: Column(
            children: <Widget>[
              Expanded(
                child: FutureBuilder(
                  future: getFirestoreDoneData(),
                  builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
                    print(snapshot);
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    } else {
                      print(snapshot);
                      final docs = snapshot.data;
                      print(docs);
                      return ListView.builder(
                          scrollDirection: Axis.vertical,
                          itemCount: snapshot.data.docs.length,
                          itemBuilder: (context, index) {
                            return Slidable(
                              child: Card(
                                child: Builder(
                                  builder: (context) => ListTile(
                                    title: Container(
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Container(
                                            margin: EdgeInsets.only(left: 5),
                                            child: Text(
                                              snapshot.data.docs[index]
                                                  ['title'],
                                              style: GoogleFonts.roboto(
                                                  fontSize: 18,
                                                  color: Colors.black),
                                            ),
                                          ),
                                          SizedBox(height: 3),
                                          Container(
                                            margin: EdgeInsets.only(left: 5),
                                            child: Text(
                                              snapshot.data.docs[index]
                                                  ['description'],
                                              style: GoogleFonts.roboto(
                                                  fontSize: 15,
                                                  color: Colors.black54),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    onTap: () {
                                      final slidable = Slidable.of(context);
                                      final isClosed = slidable.renderingMode ==
                                          SlidableRenderingMode.none;
                                      if (isClosed) {
                                        slidable.open();
                                      } else {
                                        slidable.close();
                                      }
                                    },
                                  ),
                                ),
                              ),
                              actionPane: SlidableStrechActionPane(),
                              actionExtentRatio: 0.20,
                              secondaryActions: <Widget>[
                                IconSlideAction(
                                  caption: 'Delete',
                                  color: Colors.red,
                                  icon: Icons.delete,
                                  onTap: () async {
                                    await FirebaseFirestore.instance
                                        .collection('task')
                                        .doc(FirebaseAuth
                                            .instance.currentUser.uid)
                                        .collection('DoneTask')
                                        .doc(snapshot.data.docs[index]['time'])
                                        .delete();
                                    final snackBar = SnackBar(
                                      content:
                                          Text('Task Deleted Successfully!'),
                                      duration: Duration(seconds: 2),
                                    );
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(snackBar);
                                    Navigator.pushReplacement(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                Navigation(1)));
                                    //Navigator.pop(context).then((value) => LoadDataFromFireStore());
                                  },
                                ),
                              ],
                            );
                          });
                    }
                  },
                ),
              ),
            ],
          ),
        ),
        // floatingActionButton: FloatingActionButton(
        //   onPressed: () {
        //     // Navigator.push(
        //     //     context, MaterialPageRoute(builder: (context) => AddTask()));
        //   },
        //   backgroundColor: Theme.of(context).primaryColor,
        //   child: Icon(Icons.add, color: Colors.white, size: 40),
        // ),
      ),
    );
  }

  Future<QuerySnapshot> getFirestoreDoneData() {
    return FirebaseFirestore.instance
        .collection('task')
        .doc(FirebaseAuth.instance.currentUser.uid)
        .collection('DoneTask')
        .get();
  }
}
