import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

import 'package:todo_firebase/screens/navigation.dart';

import '../dashboard/home_page.dart';
import '../task/edit_task.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  //String uid = '';

  @override
  void initState() {
    // TODO: implement initState

    // getuid();

    super.initState();
  }

  // getuid() async {
  //   FirebaseAuth auth = FirebaseAuth.instance;
  //   final User user = await auth.currentUser;
  //   setState(() {
  //     uid = user.uid;
  //     print(uid);
  //   });
  // }

  doneTask(title, description, time, startTime, endTime, status) async {
    FirebaseAuth auth = FirebaseAuth.instance;
    final User user = await auth.currentUser;
    String uid = user.uid;
    setState(() {
      status = 'Done';
    });
    await FirebaseFirestore.instance
        .collection('task')
        .doc(uid)
        .collection('DoneTask')
        .doc(time.toString())
        .set({
      'Status': status,
      'title': title,
      'description': description,
      'time': time.toString(),
      'StartTime': startTime,
      'EndTime': endTime,
    });

    var currentTime = DateTime.now();

    await FirebaseFirestore.instance
        .collection('task')
        .doc(uid)
        .collection('AllTask')
        .doc(time.toString())
        .update({
      'Status': status,
      'title': title,
      'description': description,
      'time': currentTime.toString(),
      'StartTime': startTime,
      'EndTime': endTime,
    });

    final snackBar = SnackBar(
      content: Text('Task Done Successfully!'),
      duration: Duration(seconds: 2),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);

    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => Navigation(0)));
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context) => LoadDataFromFireStore())),
      child: Scaffold(
        // appBar: AppBar(
        //   leading: IconButton(
        //     icon: Icon(Icons.arrow_back),
        //     onPressed: () {
        //       Navigator.pushReplacement(
        //           context,
        //           MaterialPageRoute(
        //               builder: (context) => LoadDataFromFireStore()));
        //     },
        //   ),
        //   centerTitle: true,
        //   title: Text("Pending Task"),
        //   actions: <Widget>[
        //     IconButton(
        //         icon: Icon(Icons.logout),
        //         onPressed: () async {
        //           await FirebaseAuth.instance.signOut();
        //         })
        //   ],
        // ),
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          // color: Colors.black,
          child: Column(
            children: <Widget>[
              Expanded(
                child: FutureBuilder(
                  future: getFirestoreData(),
                  builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
                    print(snapshot);
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    } else {
                      print(snapshot);
                      final docs = snapshot.data;
                      print(docs);
                      return ListView.builder(
                          scrollDirection: Axis.vertical,
                          itemCount: snapshot.data.docs.length,
                          itemBuilder: (context, index) {
                            return Slidable(
                              child: Card(
                                shadowColor: Colors.lightBlueAccent,
                                child: Builder(
                                  builder: (context) => ListTile(
                                    title: Container(
                                      margin: EdgeInsets.only(top: 10),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Container(
                                            margin: EdgeInsets.only(left: 5),
                                            child: Text(
                                              snapshot.data.docs[index]
                                                  ['title'],
                                              style: GoogleFonts.roboto(
                                                  fontSize: 18,
                                                  color: Colors.black),
                                            ),
                                          ),
                                          SizedBox(height: 3),
                                          Container(
                                            margin: EdgeInsets.only(left: 5),
                                            child: Text(
                                              snapshot.data.docs[index]
                                                  ['description'],
                                              style: GoogleFonts.roboto(
                                                  fontSize: 15,
                                                  color: Colors.black54),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    onTap: () {
                                      final slidable = Slidable.of(context);
                                      final isClosed = slidable.renderingMode ==
                                          SlidableRenderingMode.none;
                                      if (isClosed) {
                                        slidable.open();
                                      } else {
                                        slidable.close();
                                      }
                                    },
                                  ),
                                ),
                              ),
                              actionPane: SlidableStrechActionPane(),
                              actionExtentRatio: 0.14,
                              secondaryActions: <Widget>[
                                IconSlideAction(
                                  caption: 'Done',
                                  color: Colors.green,
                                  icon: Icons.done,
                                  onTap: () async {
                                    doneTask(
                                        snapshot.data.docs[index]['title'],
                                        snapshot.data.docs[index]
                                            ['description'],
                                        snapshot.data.docs[index]['time'],
                                        snapshot.data.docs[index]['StartTime'],
                                        snapshot.data.docs[index]['EndTime'],
                                        snapshot.data.docs[index]['Status']);

                                    await FirebaseFirestore.instance
                                        .collection('task')
                                        .doc(FirebaseAuth
                                            .instance.currentUser.uid)
                                        .collection('MyTask')
                                        .doc(snapshot.data.docs[index]['time'])
                                        .delete();
                                  },
                                ),

                                IconSlideAction(
                                  caption: 'Edit',
                                  color: Colors.amberAccent,
                                  icon: Icons.edit,
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => EditTask(
                                                  title: snapshot.data
                                                      .docs[index]['title'],
                                                  description:
                                                      snapshot.data.docs[index]
                                                          ['description'],
                                                  StartTime: snapshot.data
                                                      .docs[index]['StartTime'],
                                                  EndTime: snapshot.data
                                                      .docs[index]['EndTime'],
                                                  time: snapshot
                                                      .data.docs[index]['time'],
                                                  types: snapshot.data
                                                      .docs[index]['types'],
                                                )));
                                  },
                                ),
                                IconSlideAction(
                                  caption: 'Delete',
                                  color: Colors.red,
                                  icon: Icons.delete,
                                  onTap: () async {
                                    await FirebaseFirestore.instance
                                        .collection('task')
                                        .doc(FirebaseAuth
                                            .instance.currentUser.uid)
                                        .collection('MyTask')
                                        .doc(snapshot.data.docs[index]['time'])
                                        .delete();
                                    final snackBar = SnackBar(
                                      content:
                                          Text('Task Deleted Successfully!'),
                                      duration: Duration(seconds: 2),
                                    );
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(snackBar);
                                    Navigator.pushReplacement(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                Navigation(0)));
                                    //Navigator.pop(context).then((value) => LoadDataFromFireStore());
                                  },
                                ),
                              ],
                            );
                          });
                    }
                  },
                ),
              ),
            ],
          ),
        ),
        // floatingActionButton: FloatingActionButton(
        //   onPressed: () {
        //     // Navigator.push(
        //     //     context, MaterialPageRoute(builder: (context) => AddTask()));
        //   },
        //   backgroundColor: Theme.of(context).primaryColor,
        //   child: Icon(Icons.add, color: Colors.white, size: 40),
        // ),
      ),
    );
  }

  Future<QuerySnapshot> getFirestoreData() {
    return FirebaseFirestore.instance
        .collection('task')
        .doc(FirebaseAuth.instance.currentUser.uid)
        .collection('MyTask')
        .get();
  }

  Future<QuerySnapshot> getFirestoreDoneData() {
    return FirebaseFirestore.instance
        .collection('task')
        .doc(FirebaseAuth.instance.currentUser.uid)
        .collection('DoneTask')
        .get();
  }
}
