import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TitleField extends StatefulWidget {
  String Hint;
  String Title;

  TextEditingController _controller;
  TitleField(this.Hint,this.Title,this._controller);


  @override
  _TitleFieldState createState() => _TitleFieldState();
}

class _TitleFieldState extends State<TitleField> {


  @override
  Widget build(BuildContext context) {
    return Container(
      child: TextFormField(
        validator: (value) {
          if (value.isEmpty) {
            return 'Enter title';
          } else {
            return null;
          }
        },
          controller:widget._controller,

      style: TextStyle(color: Colors.black),
        decoration: InputDecoration(
          filled: true,
          fillColor:
          Color(0xFF0030FA).withAlpha((228 * 100).round()),
          hintText: widget.Hint,
          // labelText: 'Enter title',
          focusedErrorBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.transparent,
              ),
              borderRadius: BorderRadius.all(
                Radius.circular(18.0),
              )),
          disabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.transparent,
              ),
              borderRadius: BorderRadius.all(
                Radius.circular(18.0),
              )),
          errorBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.transparent,
              ),
              borderRadius: BorderRadius.all(
                Radius.circular(18.0),
              )),
          // enabledBorder: InputBorder.none,
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.transparent,
              ),
              borderRadius: BorderRadius.all(
                Radius.circular(18.0),
              )),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.transparent,
                width: 1,
              ),
              borderRadius: BorderRadius.all(
                Radius.circular(18.0),
              )),
        ),
      ),
    );
  }
}
