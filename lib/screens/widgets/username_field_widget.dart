import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class userNameField extends StatefulWidget {
String Hint;
String userName;
Function onSaved;
userNameField(this.Hint,this.userName,this.onSaved);

  @override
  _userNameFieldState createState() => _userNameFieldState();
}

class _userNameFieldState extends State<userNameField> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: TextFormField(

          keyboardType: TextInputType.emailAddress,
          key: ValueKey('username'),
          validator: (value) {
            if (value.isEmpty) {
              return 'Enter Username';
            }
            return null;
          },
          onSaved: widget.onSaved,
          decoration: InputDecoration(
              hintText: widget.Hint,
              hintStyle: GoogleFonts.roboto(),
              prefixIcon: new Icon(
                  Icons.person),
              border: OutlineInputBorder(
                borderRadius:
                new BorderRadius.circular(9.0),
                borderSide: new BorderSide(),
              ),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Colors.blueAccent),
                  borderRadius:
                  BorderRadius.circular(9.0)))

        //hintText: "Enter User Name",
      ),
    );
  }
}
