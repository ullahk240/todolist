import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class passwordField extends StatefulWidget {
String Hint;
String password;
Function onSaved;
passwordField(this.Hint,this.password,this.onSaved);
  @override
  _passwordFieldState createState() => _passwordFieldState();
}

class _passwordFieldState extends State<passwordField> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: TextFormField(
          obscureText: true,
          key: ValueKey('password'),
          validator: (value) {
            if (value.isEmpty || value.length < 8) {
              return 'Incorrect Password';
            }
            return null;
          },
          onSaved: widget.onSaved,
          // onSaved: (value) {
          //   _password = value;
          // },

          decoration: InputDecoration(
              hintText: widget.Hint,
              hintStyle: GoogleFonts.roboto(),
              prefixIcon: new Icon(Icons.lock),
              border: OutlineInputBorder(
                borderRadius:
                new BorderRadius.circular(9.0),
                borderSide: new BorderSide(),
              ),
              focusedBorder: OutlineInputBorder(
                  borderSide:
                  BorderSide(color: Colors.blue),
                  borderRadius:
                  BorderRadius.circular(9.0)))),
    );
  }
}
