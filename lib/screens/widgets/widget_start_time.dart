import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class startTimeField extends StatefulWidget {
String startTime;
DateFormat format;

TextEditingController _controller;
startTimeField(this.startTime,this.format,this._controller);
  @override
  _startTimeFieldState createState() => _startTimeFieldState();
}

class _startTimeFieldState extends State<startTimeField> {
  @override
  Widget build(BuildContext context) {
    return Container(

      decoration: BoxDecoration(
          color: Color(0xFF0030FA).withAlpha((228 * 100).round()),
          borderRadius: BorderRadius.all(Radius.circular(40))),
      child: DateTimeField(
        validator: (value) {
          if (value == 0) {
            return 'Select Start Time';
          } else {
            return null;
          }
        },
        format: widget.format,
        onShowPicker: (context, currentValue) async {
          final date = await showDatePicker(
              context: context,
              firstDate: DateTime.now(),
              initialDate: currentValue ?? DateTime.now(),
              lastDate: DateTime(2100));
          if (date != null) {
            final time = await showTimePicker(
              context: context,
              initialTime: TimeOfDay.fromDateTime(
                  currentValue ?? DateTime.now()),
            );
            return DateTimeField.combine(date, time);
          } else {
            return currentValue;
          }
        },
        controller: widget._controller,
        textAlign: TextAlign.center,
        decoration: InputDecoration(hintText: 'Pick Start Date'),
      ),
    );
  }
}
