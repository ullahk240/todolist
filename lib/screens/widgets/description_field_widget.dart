import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DescriptionField extends StatefulWidget {
String Hint;
String Description;
TextEditingController _controller;
DescriptionField(this.Hint,this.Description,this._controller);
  @override
  _DescriptionFieldState createState() => _DescriptionFieldState();
}

class _DescriptionFieldState extends State<DescriptionField> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: TextFormField(
        controller: widget._controller,
        validator: (value) {
          if (value.isEmpty) {
            return 'Enter description';
          } else {
            return null;
          }
        },
        style: TextStyle(color: Colors.black),
        decoration: InputDecoration(
          filled: true,
          fillColor:
          Color(0xFF0030FA).withAlpha((228 * 100).round()),
          hintText: widget.Hint,
          // labelText: 'Enter title',
          focusedErrorBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.transparent,
              ),
              borderRadius: BorderRadius.all(
                Radius.circular(18.0),
              )),
          disabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.transparent,
              ),
              borderRadius: BorderRadius.all(
                Radius.circular(18.0),
              )),
          errorBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.transparent,
              ),
              borderRadius: BorderRadius.all(
                Radius.circular(18.0),
              )),
          // enabledBorder: InputBorder.none,
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.transparent,
              ),
              borderRadius: BorderRadius.all(
                Radius.circular(18.0),
              )),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.transparent,
                width: 1,
              ),
              borderRadius: BorderRadius.all(
                Radius.circular(18.0),
              )),
        ),
        maxLines: 4,
      ),
    );
  }
}
