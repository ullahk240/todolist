import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:validators/validators.dart';

class emailField extends StatefulWidget {
String Hint;
String email;
Function onSaved;
emailField(this.Hint,this.email,this.onSaved);

  @override
  _emailFieldState createState() => _emailFieldState();
}

class _emailFieldState extends State<emailField> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: TextFormField(
        keyboardType: TextInputType.emailAddress,
        key: ValueKey('email'),
        validator: (val) => !isEmail(val) ? "Invalid Email" : null,
        onSaved: widget.onSaved,

        // onSaved: (value) {
        //   _email= value;
        // },
        decoration: InputDecoration(
          hintText: widget.Hint,
          hintStyle: GoogleFonts.roboto(),
          prefixIcon: new Icon(Icons.email,),
          border: OutlineInputBorder(
            borderRadius: new BorderRadius.circular(9.0),
            borderSide: new BorderSide(),
          ),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.blue),
              borderRadius: BorderRadius.circular(9.0)),
        ),
      ),
    );
  }
}
