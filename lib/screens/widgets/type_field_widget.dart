import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TypeField extends StatefulWidget {
  String Hint;
  String Type;
  TextEditingController _controller;
  Function onPressed;
  TypeField(this.Hint,this.Type,this._controller,this.onPressed);

  @override
  _TypeFieldState createState() => _TypeFieldState();
}

class _TypeFieldState extends State<TypeField> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: TextFormField(
        controller: widget._controller,
        readOnly: true,
        onTap: widget.onPressed,
        validator: (value) {
          if (value.isEmpty) {
            return 'Please Select One Category';
          }
          return null;
        },
        style: TextStyle(color: Colors.black),
        decoration: InputDecoration(
          filled: true,
          fillColor:
          Color(0xFF0030FA).withAlpha((228 * 100).round()),
          hintText: widget.Hint,
          // labelText: 'Enter title',
          focusedErrorBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.transparent,
              ),
              borderRadius: BorderRadius.all(
                Radius.circular(18.0),
              )),
          disabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.transparent,
              ),
              borderRadius: BorderRadius.all(
                Radius.circular(18.0),
              )),
          errorBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.transparent,
              ),
              borderRadius: BorderRadius.all(
                Radius.circular(18.0),
              )),
          // enabledBorder: InputBorder.none,
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.transparent,
              ),
              borderRadius: BorderRadius.all(
                Radius.circular(18.0),
              )),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.transparent,
                width: 1,
              ),
              borderRadius: BorderRadius.all(
                Radius.circular(18.0),
              )),
        ),
      ),
    );
  }
}
