import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:todo_firebase/main.dart';
import 'package:todo_firebase/screens/widgets/datePicker.dart';
import 'package:todo_firebase/screens/widgets/description_field_widget.dart';
import 'package:todo_firebase/screens/widgets/time%20picker/timePicker.dart';
import 'package:todo_firebase/screens/widgets/title_field_widget.dart';
import 'package:todo_firebase/screens/widgets/type_field_widget.dart';
import 'package:todo_firebase/screens/widgets/widget_end_time.dart';
import 'package:todo_firebase/screens/widgets/widget_start_time.dart';

import '../dashboard/home_page.dart';

class AddTask extends StatefulWidget {
  @override
  _AddTaskState createState() => _AddTaskState();
}

class _AddTaskState extends State<AddTask> {
  FlutterLocalNotificationsPlugin flutterLocalNotification;

  final _formKey = GlobalKey<FormState>();
  TextEditingController titleController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController startTimeController = TextEditingController();
  TextEditingController endTimeController = TextEditingController();
  TextEditingController dateController = TextEditingController();
  TextEditingController typeController = TextEditingController();

  DateTime _eventDate;
  DateTime _eventEndDate;
  DateTime selectedDate;
  String status = 'pending';
  int selected = 0;

  addTask() async {
    DateTime startDate = DateTime.parse(startTimeController.text);
    DateTime endDate = DateTime.parse(endTimeController.text);
    DateTime now = DateTime.now();
    print(now);
    print(startDate);
    print(endDate);

    startDate = DateTime(startDate.year, startDate.month, startDate.day,
        startDate.hour, startDate.minute);
    endDate = DateTime(
        endDate.year, endDate.month, endDate.day, endDate.hour, endDate.minute);
    now = DateTime(now.year, now.month, now.day, now.hour, now.minute);

    int startDateDifference = now.difference(startDate).inDays;
    int endDateDifference = now.difference(endDate).inDays;

    int startSameDateDifference = now.difference(startDate).inMinutes;
    int endSameDateDifference = now.difference(endDate).inMinutes;

    print(startSameDateDifference);
    print(endSameDateDifference);

    if (startDateDifference > 0) {
      final snackBar = SnackBar(
        content: Text('Enter the right start Date!'),
        duration: Duration(seconds: 4),
        backgroundColor: Colors.red,
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);

      startTimeController.text = '';

      return AddTask();
    } else if (endDateDifference > 0) {
      final snackBar = SnackBar(
        content: Text('Enter the right end Date!'),
        duration: Duration(seconds: 4),
        backgroundColor: Colors.red,
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);

      //startTimeController.text = '';
      endTimeController.text = '';

      return AddTask();
    } else if (startDateDifference < endDateDifference) {
      final snackBar = SnackBar(
        content: Text('Enter the right Start Date or End Date!'),
        duration: Duration(seconds: 4),
        backgroundColor: Colors.red,
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);

      startTimeController.text = '';
      endTimeController.text = '';

      return AddTask();
    } else if (startDateDifference == endDateDifference) {
      if (startSameDateDifference >= 0) {
        final snackBar = SnackBar(
          content: Text('Enter the right Start Date!'),
          duration: Duration(seconds: 4),
          backgroundColor: Colors.red,
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);

        startTimeController.text = '';
      } else if (endSameDateDifference >= 0) {
        final snackBar = SnackBar(
          content: Text('Enter the right End Date!'),
          duration: Duration(seconds: 4),
          backgroundColor: Colors.red,
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);

        endTimeController.text = '';
      } else {
        final validity = _formKey.currentState.validate();
        if (validity) {
          FirebaseAuth auth = FirebaseAuth.instance;
          final User user = await auth.currentUser;
          String uid = user.uid;
          var time = DateTime.now();
          await FirebaseFirestore.instance
              .collection('task')
              .doc(uid)
              .collection('MyTask')
              .doc(time.toString())
              .set({
            'Status': status,
            'title': titleController.text,
            'description': descriptionController.text,
            'types': typeController.text,
            'time': time.toString(),
            'StartTime': startTimeController.text,
            'EndTime': endTimeController.text,
          });

          await FirebaseFirestore.instance
              .collection('task')
              .doc(uid)
              .collection('AllTask')
              .doc(time.toString())
              .set({
            'Status': status,
            'title': titleController.text,
            'description': descriptionController.text,
            'types': typeController.text,
            'time': time.toString(),
            'StartTime': startTimeController.text,
            'EndTime': endTimeController.text,
          });
          final snackBar = SnackBar(
            content: Text('Task Added Successfully!'),
            duration: Duration(seconds: 2),
          );
          ScaffoldMessenger.of(context).showSnackBar(snackBar);

          titleController.text = '';
          descriptionController.text = '';
          startTimeController.text = '';
          endTimeController.text = '';
          typeController.text = '';

          var androidDetails = new AndroidNotificationDetails(
              "channelId", "ToDo List", "Task Addition",
              importance: Importance.max);
          var iSODetails = new IOSNotificationDetails();

          var generalNotificationDetails =
              new NotificationDetails(android: androidDetails, iOS: iSODetails);

          await flutterLocalNotification.show(
              0, "Task", "New Task Added", generalNotificationDetails);

          print("stdiff:${(-1) * startDateDifference}");

          var schedule = DateTime.now().add(Duration(
              days: (-1) * (startDateDifference),
              minutes: (-1) * (startSameDateDifference)));

          print(schedule);

          await flutterLocalNotification.schedule(1, "Task",
              "Your task time started", schedule, generalNotificationDetails);

          Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (context) => LoadDataFromFireStore()));
        }
      }
    } else {
      final validity = _formKey.currentState.validate();
      if (validity) {
        FirebaseAuth auth = FirebaseAuth.instance;
        final User user = await auth.currentUser;
        String uid = user.uid;
        var time = DateTime.now();
        await FirebaseFirestore.instance
            .collection('task')
            .doc(uid)
            .collection('MyTask')
            .doc(time.toString())
            .set({
          'Status': status,
          'title': titleController.text,
          'description': descriptionController.text,
          'time': time.toString(),
          'types': typeController.text,
          'StartTime': startTimeController.text,
          'EndTime': endTimeController.text,
        });

        await FirebaseFirestore.instance
            .collection('task')
            .doc(uid)
            .collection('AllTask')
            .doc(time.toString())
            .set({
          'Status': status,
          'title': titleController.text,
          'description': descriptionController.text,
          'types': typeController.text,
          'time': time.toString(),
          'StartTime': startTimeController.text,
          'EndTime': endTimeController.text,
        });
        final snackBar = SnackBar(
          content: Text('Task Added Successfully!'),
          duration: Duration(seconds: 2),
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);

        titleController.text = '';
        descriptionController.text = '';
        startTimeController.text = '';
        endTimeController.text = '';
        typeController.text = '';

        var androidDetails = new AndroidNotificationDetails(
            "channelId", "ToDo List", "Task Addition",
            importance: Importance.max);
        var iSODetails = new IOSNotificationDetails();

        var generalNotificationDetails =
            new NotificationDetails(android: androidDetails, iOS: iSODetails);

        await flutterLocalNotification.show(
            0, "Task", "New Task Added", generalNotificationDetails);

        var schedule = DateTime.now().add(Duration(
            days: (-1) * (startDateDifference),
            minutes: (-1) * (startSameDateDifference)));

        print(schedule);
        await flutterLocalNotification.schedule(1, "Task",
            "Your task time started", schedule, generalNotificationDetails);

        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) => LoadDataFromFireStore()));
      }
    }
  }

  final items = [
    'Work',
    'Home',
    'Office',
  ];

  @override
  void initState() {
    // TODO: implement initState

    setState(() {
      _eventDate = DateTime.now();
      selectedDate = DateTime.now();
    });

    //local notification setup

    var androidInitialize = new AndroidInitializationSettings('app_icon');
    var iOSInitialize = new IOSInitializationSettings();
    var initializationsSettings = new InitializationSettings(
        android: androidInitialize, iOS: iOSInitialize);
    flutterLocalNotification = new FlutterLocalNotificationsPlugin();
    flutterLocalNotification.initialize(initializationsSettings,
        onSelectNotification: notificationSelected);

    super.initState();
  }

  _bottomSheet() {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
            height: MediaQuery.of(context).size.height * 0.3,
            child: Column(
              children: <Widget>[
                Align(
                  alignment: Alignment.topRight,
                  child: TextButton(
                    child: Text("Done"),
                    onPressed: () {
                      if (typeController.text.isEmpty) {
                        typeController.text = 'Work';
                        Navigator.of(context).pop();
                      } else {
                        Navigator.of(context).pop();
                      }
                    },
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8, right: 8),
                    // color: Colors.blue,
                    child: CupertinoPicker(
                      onSelectedItemChanged: (int i) {
                        setState(() {
                          typeController.text = items[i];
                        });
                      },
                      useMagnifier: true,
                      squeeze: 1.4,
                      magnification: 1.1,
                      backgroundColor: Colors.white,
                      children: items
                          .map((items) => Center(
                                child: Text(items),
                              ))
                          .toList(),
                      itemExtent: 30,
                      looping: false,
                    ),
                  ),
                )
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    final format = DateFormat("yyyy-MM-dd HH:mm:ss");

    return Scaffold(
      appBar: AppBar(
        title: Text('Add Task'),
        centerTitle: true,
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(top: 30, right: 10, left: 10),
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(right: 270),
                  child: Text(
                    "Title",
                    style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),
                  ),
                ),

                Container(
                  margin: EdgeInsets.all(10),
                  child: TitleField("Enter Title", titleController.text, titleController),
                ),

                Padding(
                  padding: const EdgeInsets.only(right: 227, top: 12),
                  child: Text(
                    "Category",
                    style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(10),
                  child: TypeField('Select Category', typeController.text, typeController, _bottomSheet),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 213, top: 12),
                  child: Text(
                    "Description",
                    style: TextStyle(
                      fontSize: 23,
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(10),
                  child: DescriptionField("Enter Description", descriptionController.text, descriptionController),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 213, top: 12),
                  child: Text(
                    "Start Date",
                    style: TextStyle(
                      fontSize: 23,
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  child: startTimeField(startTimeController.text, format, startTimeController),

                ),
                Padding(
                  padding: const EdgeInsets.only(right: 213, top: 12),
                  child: Text(
                    "End Date",
                    style: TextStyle(
                      fontSize: 23,
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  child: endTimeField(endTimeController.text, format, endTimeController),
                ),

                Container(
                  margin: EdgeInsets.only(top: 20),
                  width: 150,
                  height: 60,
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.resolveWith<Color>(
                          (Set<MaterialState> states) {
                        if (states.contains(MaterialState.pressed))
                          return Colors.blueAccent;
                        return Theme.of(context).primaryColor;
                      }),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(39.0),
                              side: BorderSide(
                                  color: Colors.transparent, width: 2.0))),
                    ),
                    child: Text(
                      "Add",
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    onPressed: () {
                      addTask();
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future notificationSelected(String payload) async {
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => MyApp()));
  }
}
