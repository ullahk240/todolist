// import 'dart:html';
//
// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:firebase_auth/firebase_auth.dart';
//import 'package:firebase_core/firebase_core.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:image_picker/image_picker.dart';

import 'dart:io';

import 'package:flutter/rendering.dart';
import 'package:http/http.dart' as http;

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:page_transition/page_transition.dart';
import 'package:todo_firebase/screens/profile/profile_Screen.dart';

class EditProfile extends StatefulWidget {
  EditProfile({this.name, this.email, this.password, this.imageUrl});

  final String name;
  final String email;
  final String password;
  final String imageUrl;

  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _nameController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passController = TextEditingController();

  String url = "";

  @override
  void initState() {
    // TODO: implement initState

    setState(() {
      url ='${widget.imageUrl}';
      _nameController.text = widget.name;
      _emailController.text = widget.email;
      _passController.text = widget.password;
    });

    super.initState();
  }

  FirebaseAuth auth = FirebaseAuth.instance;
  User user = FirebaseAuth.instance.currentUser;
  updateProfile(name, email, password) async {
    String path = DateTime.now().toString();
    var imageFile = FirebaseStorage.instance.ref().child(path).child("/.jpg");
    UploadTask task = imageFile.putFile(file);
    TaskSnapshot snapshot = await task;

    url = await snapshot.ref.getDownloadURL();
    print(url);

    final validity = _formKey.currentState.validate();
    if (validity) {
      await FirebaseFirestore.instance
          .collection('users')
          .doc(FirebaseAuth.instance.currentUser.uid)
          .update({
        'username': name,
        'email': email,
        'password': password,
        'imageUrl': url
      });
      User user = FirebaseAuth.instance.currentUser;
      user.updateEmail(email).then((value) {
        email:
        email;
      });
      user.updatePassword(password).then((value) {
        password:
        password;
      });
      final snackBar = SnackBar(
        content: Text('Profile Updated Successfully!'),
        backgroundColor: Colors.green,
        duration: Duration(seconds: 2),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
      return
        Navigator.pushReplacement(context,PageTransition(type:
        PageTransitionType.fade,
            child: Profile()));
    }
  }

  final ImagePicker image = ImagePicker();
  File file;

  getImage() async {
    var img = await image.pickImage(source: ImageSource.gallery);
    setState(() {
      file = File(img.path);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit Profile"),
        shadowColor: Colors.transparent,
        centerTitle: false,        actions: <Widget>[
        // IconButton(
        //     icon: Icon(Icons.check),
        //     onPressed: () {
        //       updateProfile(_nameController.text, _emailController.text,
        //           _passController.text);
        //     })
      ],
      ),
      body: Padding(
        padding: EdgeInsets.only(
            left: 20,
            right: 20,
            top: MediaQuery.of(context).size.height * 0.1),
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Padding(
              padding: const EdgeInsets.all(13.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      getImage();
                    },
                    child: CircleAvatar(
                      radius: 80,
                      backgroundImage: file == null && url == 'false'
                          ? AssetImage("assets/images/avat.png") : url != 'false' ? NetworkImage(url)
                          : FileImage(File(file.path)) as ImageProvider,
                      child: Stack(
                        children: [
                          Align(
                              alignment: Alignment.bottomRight,
                              child: Padding(
                                padding: const EdgeInsets.only(right: 19.0, bottom: 10),
                                child: CircleAvatar(
                                  radius: 18,
                                  backgroundColor: Colors.white,
                                  child: Icon(CupertinoIcons.camera),
                                ),
                              )),
                        ],
                      ),
                    ),
                  ),

                  SizedBox(
                    height: 10,
                  ),

                  Container(
                    child: TextFormField(
                        controller: _nameController,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Enter Name';
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            hintText: "Enter Name",
                            hintStyle: GoogleFonts.roboto(),
                            prefixIcon: new Icon(
                                Icons.person),
                            border: OutlineInputBorder(
                              borderRadius:
                              new BorderRadius.circular(9.0),
                              borderSide: new BorderSide(),
                            ),
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.blueAccent),
                                borderRadius:
                                BorderRadius.circular(9.0)))
                    ),
                  ),
                  SizedBox(height: 10),

                  Container(
                    child: TextFormField(
                        controller: _emailController,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Enter Email';
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            hintText: "Enter Email",
                            hintStyle: GoogleFonts.roboto(),
                            prefixIcon: new Icon(
                              Icons.email,),
                            border: OutlineInputBorder(
                              borderRadius:
                              new BorderRadius.circular(9.0),
                              borderSide: new BorderSide(),
                            ),
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.blueAccent),
                                borderRadius:
                                BorderRadius.circular(9.0)))
                    ),
                  ),
                  SizedBox(height: 10),
                  Container(
                    child: TextFormField(
                        controller: _passController,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Enter Password';
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            hintText: "Enter Password",
                            hintStyle: GoogleFonts.roboto(),
                            prefixIcon: new Icon(Icons.lock),
                            border: OutlineInputBorder(
                              borderRadius:
                              new BorderRadius.circular(9.0),
                              borderSide: new BorderSide(),
                            ),
                            focusedBorder: OutlineInputBorder(
                                borderSide:
                                BorderSide(color: Colors.blue),
                                borderRadius:
                                BorderRadius.circular(9.0)))
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                      padding: EdgeInsets.all(5),
                      width: double.infinity,
                      height: 60,
                      child: RaisedButton(
                          color: Colors.blueAccent,
                          child: Text(
                            'Update Profile',
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(9),
                          ),
                          onPressed: () {
                            updateProfile(
                                _nameController.text, _emailController.text,
                                _passController.text);
                          }
                      )
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

