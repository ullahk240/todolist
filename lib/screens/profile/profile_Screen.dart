import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:line_icons/line_icon.dart';
import 'package:page_transition/page_transition.dart';
import 'edit_profile.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Profile"),
        shadowColor: Colors.transparent,
        centerTitle: false,
      ),
      body: Padding(
          padding: EdgeInsets.only(
              left: 30,
              right: 30,
              top: MediaQuery.of(context).size.height * 0.1),
          child: Container(
            child: StreamBuilder(
              stream: FirebaseFirestore.instance
                  .collection('users')
                  .doc(FirebaseAuth.instance.currentUser.uid)
                  .snapshots(),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                } else {
                  return Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        CircleAvatar(
                            radius: 80,
                            backgroundColor: Colors.white,
                            backgroundImage: snapshot.data['imageUrl'] == "https://bit.ly/3iEFYdG" ? AssetImage("assets/images/avat.png")
                                 : NetworkImage(snapshot.data['imageUrl']) ),
                        SizedBox(
                          height: 10,

                        ),
                        Container(
                          child: TextFormField(
                            readOnly: true,
                              decoration: InputDecoration(
                                  hintText: '${snapshot.data['username']}',
                                  hintStyle: GoogleFonts.roboto(),
                                  prefixIcon: new Icon(
                                      Icons.person),
                                  border: OutlineInputBorder(
                                    borderRadius:
                                    new BorderRadius.circular(9.0),
                                    borderSide: new BorderSide(),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.blueAccent),
                                      borderRadius:
                                      BorderRadius.circular(9.0)))
                          ),
                        ),
                        SizedBox(height: 10),
                        Container(
                          child: TextFormField(
                              readOnly: true,
                              decoration: InputDecoration(
                                  hintText:'${snapshot.data['email']}',
                                  hintStyle: GoogleFonts.roboto(),
                                  prefixIcon: new Icon(
                                    Icons.email,),
                                  border: OutlineInputBorder(
                                    borderRadius:
                                    new BorderRadius.circular(9.0),
                                    borderSide: new BorderSide(),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.blueAccent),
                                      borderRadius:
                                      BorderRadius.circular(9.0)))
                          ),
                        ),
                        SizedBox(height: 10),
                        Container(
                          child: TextFormField(
                              readOnly: true,
                              decoration: InputDecoration(
                                  hintText:'${snapshot.data['password']}',
                                  hintStyle: GoogleFonts.roboto(),
                                  prefixIcon: new Icon(Icons.lock),
                                  border: OutlineInputBorder(
                                    borderRadius:
                                    new BorderRadius.circular(9.0),
                                    borderSide: new BorderSide(),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                      borderSide:
                                      BorderSide(color: Colors.blue),
                                      borderRadius:
                                      BorderRadius.circular(9.0)))
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          padding: EdgeInsets.all(5),
                          width: double.infinity,
                          height: 60,
                          child: RaisedButton(
                            color: Colors.blueAccent,
                            child: Text(
                              'Edit Profile',
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(9),
                            ),
                            onPressed: () {
                              Navigator.push(context,PageTransition(type:
                                  PageTransitionType.fade,
                                  child: EditProfile(
                                    imageUrl: snapshot.data['imageUrl'],
                                    name: snapshot.data['username'],
                                    email: snapshot.data['email'],
                                    password: snapshot.data['password'],
                                  ),
                                 ));
                            },
                          ),
                        ),
                      ],
                    ),
                  );
                }
              },
            ),
          )),
    );
  }
}
