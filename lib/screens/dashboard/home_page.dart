import 'dart:io';
import 'dart:math';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:ntp/ntp.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:intl/intl.dart';
import 'package:todo_firebase/auth/authForm.dart';
import 'package:todo_firebase/auth/authscreen.dart';
import 'package:todo_firebase/main.dart';
import 'package:todo_firebase/screens/navigation.dart';
import '../profile/edit_profile.dart';
import '../listing/home.dart';
import '../profile/profile_Screen.dart';
import '../task/add_task.dart';

class LoadDataFromFireStore extends StatefulWidget {
  @override
  _LoadDataFromFireStoreState createState() => _LoadDataFromFireStoreState();
}

class _LoadDataFromFireStoreState extends State<LoadDataFromFireStore> {
  List<Color> _colorCollection = <Color>[];
  List<DateTime> _blackoutDates = <DateTime>[];

  MeetingDataSource events;
  final List<String> options = <String>['Add', 'Delete', 'Update'];
  final databaseReference = FirebaseFirestore.instance;
  var imageUrlSnapshot;
  //String uid = 'sMaUKwnpkeTKxmP4pAs0jZsuMSR2';

  @override
  void initState() {
    // TODO: implement initState

    //getuid();
    _initializeEventColor();
    setState(() {
      getDataFromFireStore();
      getImageFromFireStore();
    });

    super.initState();
  }
  Future getImageFromFireStore() async {
    imageUrlSnapshot = await databaseReference
        .collection('users')
        .doc(FirebaseAuth.instance.currentUser.uid).snapshots();
    print("imageUrlSnapshot:::${imageUrlSnapshot}");
  }
  // getuid() async {
  //   FirebaseAuth auth = FirebaseAuth.instance;
  //   final User user = await auth.currentUser;
  //   setState(() {
  //     uid = user.uid;
  //   });
  // }

  Future <String> getImageurl() async {
    return await imageUrlSnapshot.data['imageUrl'];

  }
  Future<void> getDataFromFireStore() async {
    FirebaseAuth auth = FirebaseAuth.instance;
    final User user = auth.currentUser;
    String uid = user.uid;
    DateTime now = DateTime.now();
    var snapShotsValue = await databaseReference
        .collection('task')
        .doc(uid)
        .collection('MyTask')
        .get();
    print(uid);

    List<Meeting> list = snapShotsValue.docs
        .map((e) => Meeting(
        taskTitle: e.data()['title'],
        time: e.data()['time'],
        description: e.data()['description'],
        startTime:
        DateFormat('yyyy-MM-dd HH:mm:ss').parse(e.data()['StartTime']),
        endTime:
        DateFormat('yyyy-MM-dd HH:mm:ss').parse(e.data()['EndTime']),
        background: color(
            DateFormat('yyyy-MM-dd HH:mm:ss').parse(e.data()['StartTime']),
            DateFormat('yyyy-MM-dd HH:mm:ss').parse(e.data()['EndTime']),
            e.data()['Status'],
            DateFormat('yyyy-MM-dd HH:mm:ss').parse(e.data()['time'])),
        isAllDay: false,
        status: e.data()['Status']))
        .toList();
    setState(() {
      events = MeetingDataSource(list);
    });
  }

  Future<bool> _onBackPressed() {
    return showDialog(
        context: context,
        builder: (context) => CupertinoAlertDialog(
          title: new Text("Exit"),
          content: new Text("Are you sure you want to exit\n"
              "from the app?"
          ),
          actions: <Widget>[
            CupertinoDialogAction(
              child: Text("Cancel",
              ),
              onPressed: () => Navigator.of(context).pop(true),
            ),
            CupertinoDialogAction(
              child: Text("Exit",
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Colors.red,
                  )),
              onPressed: ()=> exit(0),
            )
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {


    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
          appBar: AppBar(
            leading:
            GestureDetector(
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: StreamBuilder(
                  stream: FirebaseFirestore.instance.collection("users").doc(FirebaseAuth.instance.currentUser.uid).snapshots(),
                  builder: (context, snapshot) {
                    if(snapshot.connectionState == ConnectionState.waiting)
                    {
                      return CircleAvatar(
                          radius: 3,
                          child: Image.asset('assets/images/avat.png',  width: 50,
                            height: 50,
                            fit: BoxFit.cover,));
                    }else {
                      if(snapshot.hasData ) {
                        if(snapshot.data['imageUrl'] == "https://bit.ly/3iEFYdG") {
                          return ClipOval(
                            child: Image.asset('assets/images/avat.png', width: 50,
                              height: 50,
                              fit: BoxFit.cover,), );
                        }else {
                          return ClipOval(
                              child: Image.network(
                                snapshot.data['imageUrl'], width: 50,
                                height: 50,
                                fit: BoxFit.cover,));
                        }
                      }else {
                        return CircleAvatar(
                            radius: 3,
                            child: Image.asset('assets/images/avat.png',  width: 50,
                              height: 50,
                              fit: BoxFit.cover,));
                      }
                    }
                  },
                ),
              ),
              onTap: (){
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => Profile()));
              },
            ),
            title: Text('ToDo'),
            // // centerTitle: true,
            // title: Padding(
            //   padding: const EdgeInsets.only(left: 8.0),
            //   child: Container(
            //     width: 40,
            //     child: GestureDetector(
            //       onTap: () {
            //         Navigator.of(context)
            //             .push(MaterialPageRoute(builder: (context) => Profile()));
            //       },
            //
            //
            //       child: CircleAvatar(
            //         radius: 20,
            //         backgroundColor: Colors.white,
            //            backgroundImage: AssetImage('assets/images/avat.png'),
            //
            //            //imageUrlSnapshot.data['imageUrl'] == false ? AssetImage("assets/images/avat.png")
            //              //  : NetworkImage(imageUrlSnapshot.data['imageUrl'])
            //       ),
            //     ),
            //   ),
            // ),
            actions: <Widget>[
              IconButton(
                  icon: Icon(Icons.all_inbox),
                  onPressed: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => Navigation(0)));
                  }),
              IconButton(
                icon: Icon(Icons.logout),
                onPressed: () async {
                  showDialog(
                      context: context,
                      builder: (context) => CupertinoAlertDialog(
                        title: new Text("Logout"),
                        content: new Text("Are you sure you want to Logout\n from the app?"
                        ),
                        actions: <Widget>[
                          CupertinoDialogAction(
                              child: Text("Logout",
                                  style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    color: Colors.red,
                                  )),
                              onPressed: () {
                                FirebaseAuth.instance.signOut();
                                return Navigator.of(context)
                                    .push(MaterialPageRoute(builder: (context) => AuthScreen()));
                              }
                          ),
                          CupertinoDialogAction(
                            child: Text("Cancel",
                            ),
                            onPressed: () => Navigator.of(context).pop(true),
                          ),
                        ],
                      ));
                },
              ) ],
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => AddTask()));
            },
            backgroundColor: Theme.of(context).primaryColor,
            child: Icon(Icons.add, color: Colors.white, size: 40),
          ),
          body: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: SfCalendar(
              minDate: DateTime(2021, 09, 01, 9, 0, 0),
              blackoutDates:
              _blackOutDates(DateTime(2021, 09, 01, 9, 0, 0).toString()),
              // onViewChanged: viewChanged,
              blackoutDatesTextStyle: TextStyle(
                  fontWeight: FontWeight.w400,
                  fontSize: 13,
                  color: Colors.pinkAccent,
                  decoration: TextDecoration.lineThrough),
              showNavigationArrow: true,
              //showWeekNumber: true,
              weekNumberStyle:
              WeekNumberStyle(backgroundColor: Colors.blueAccent),
              view: CalendarView.month,
              initialDisplayDate: DateTime.now(),
              dataSource: events,
              monthViewSettings: MonthViewSettings(
                  appointmentDisplayMode:
                  MonthAppointmentDisplayMode.appointment,
                  showAgenda: true,
                  agendaStyle: AgendaStyle()),
              //onTap: calendarTapped,
            ),
          )),
    );
  }

  // void viewChanged(ViewChangedDetails viewChangedDetails) {
  //   List<DateTime> _blackoutDateCollection = <DateTime>[];
  //
  //   var endDate = DateTime(2020 - 10 - 10);
  //   var currentDate = DateTime.now();
  //
  //   for (int i = 0; i < endDate.difference(currentDate).inDays; i++) {
  //     print(i);
  //
  //     _blackoutDateCollection.add(endDate.add(Duration(days: 1)));
  //   }
  //   setState(() {
  //     _blackoutDates = _blackoutDateCollection;
  //   });
  // }

  // void calendarTapped(CalendarTapDetails details) {
  //   if (details.appointments != null && details.appointments.length > 0) {
  //     showDialog(
  //         context: context,
  //         builder: (context) => AlertDialog(
  //               content: Container(
  //                 height: MediaQuery.of(context).size.height / 2,
  //                 width: double.maxFinite,
  //                 child: Column(
  //                   children: <Widget>[
  //                     Align(
  //                       alignment: Alignment.centerRight,
  //                       child: ElevatedButton(
  //                         child: Text("Add Task"),
  //                         onPressed: () {
  //                           Navigator.push(
  //                               context,
  //                               MaterialPageRoute(
  //                                   builder: (context) => AddTask()));
  //                         },
  //                       ),
  //                     ),
  //                     Expanded(
  //                         child: ListView.builder(
  //                             shrinkWrap: true,
  //                             itemCount: details.appointments.length,
  //                             itemBuilder: (context, index) {
  //                               return Column(
  //                                 children: <Widget>[
  //                                   ListTile(
  //                                     title: Text(
  //                                         "\n Title: ${details.appointments[index].taskTitle}"),
  //                                     subtitle: Text(
  //                                         "\n Desc: ${details.appointments[index].description}"
  //                                         "\nStart Time: ${DateFormat.yMMMd().format(details.appointments[index].startTime)}"
  //                                         "\nEnd Time : ${DateFormat.yMMMd().format(details.appointments[index].endTime)}"),
  //                                   ),
  //                                 ],
  //                               );
  //                             })),
  //                     Row(
  //                       mainAxisAlignment: MainAxisAlignment.end,
  //                       crossAxisAlignment: CrossAxisAlignment.end,
  //                       children: <Widget>[
  //                         ElevatedButton(
  //                             onPressed: () {
  //                               Navigator.of(context).pop();
  //                             },
  //                             child: Text("Close"))
  //                       ],
  //                     )
  //                   ],
  //                 ),
  //               ),
  //             ));
  //   }
  // }

  void _initializeEventColor() {
    _colorCollection.add(const Color(0xFF0F8644));
    _colorCollection.add(const Color(0xFF8B1FA9));
    _colorCollection.add(const Color(0xFFD20100));
    _colorCollection.add(const Color(0xFFFC571D));
    _colorCollection.add(const Color(0xFF36B37B));
    _colorCollection.add(const Color(0xFF01A1EF));
    _colorCollection.add(const Color(0xFF3D4FB5));
    _colorCollection.add(const Color(0xFFE47C73));
    _colorCollection.add(const Color(0xFF636363));
    _colorCollection.add(const Color(0xFF0A8043));
  }

  Color color(
      DateTime startTime, DateTime endTime, String status, DateTime time) {
    DateTime now = DateTime.now();

    startTime = DateTime(startTime.year, startTime.month, startTime.day,
        startTime.hour, startTime.minute);
    endTime = DateTime(
        endTime.year, endTime.month, endTime.day, endTime.hour, endTime.minute);
    now = DateTime(now.year, now.month, now.day, now.hour, now.minute);

    int startDateDifference = now.difference(startTime).inDays;
    int endDateDifference = now.difference(endTime).inDays;

    int startMinDifference = now.difference(startTime).inMinutes;
    int endMinDifference = now.difference(endTime).inMinutes;

    print(startTime);
    print(endTime);
    print(now);
    print("startDatDiff:${startDateDifference}");
    print("startMinDiff:${startMinDifference}");
    print("endDatDiff:${endDateDifference}");
    print("endDatDiff:${endMinDifference}");

    if (startDateDifference == endDateDifference) {
      if (endMinDifference >= 0) {
        return Colors.red;
      } else if (startMinDifference < 0) {
        return Colors.purple;
      } else {
        return Colors.green;
      }
    } else if (endDateDifference > 0) {
      return Colors.red;
    } else if (startDateDifference < 0) {
      return Colors.purple;
    } else {
      return Colors.green;
    }
  }

  List _blackOutDates(stTime) {
    var startTime = DateFormat('yyyy-MM-dd HH:mm:ss').parse(stTime);
    startTime = DateTime(startTime.day);
    var currentDate = DateTime.now();
    var currentDateNtp = NTP.now();

    print("currentDateNtp:::${currentDateNtp.toString()}");

    currentDate = DateTime(currentDate.day);

    var dateDifference = currentDate.difference(startTime).inDays;

    print(dateDifference);

    final items = List<DateTime>.generate(
        dateDifference,
            (index) => DateTime.utc(
          DateTime.now().year,
          DateTime.now().month,
          DateTime.now().subtract(Duration(days: 1)).day,
        ).subtract(Duration(days: index)));

    print(items);

    return items;
  }
}



class MeetingDataSource extends CalendarDataSource {
  MeetingDataSource(List<Meeting> source) {
    appointments = source;
  }

  @override
  DateTime getStartTime(int index) {
    return appointments[index].startTime;
  }

  @override
  DateTime getEndTime(int index) {
    return appointments[index].endTime;
  }

  @override
  bool isAllDay(int index) {
    return appointments[index].isAllDay;
  }

  @override
  String getSubject(int index) {
    return appointments[index].taskTitle;
  }

  @override
  Color getColor(int index) {
    return appointments[index].background;
  }
}

class Meeting extends ChangeNotifier {
  String taskTitle;
  String description;
  String time;
  DateTime startTime;
  DateTime endTime;
  Color background;
  bool isAllDay;
  String status;

  Meeting(
      {this.taskTitle,
        this.description,
        this.time,
        this.startTime,
        this.endTime,
        this.background,
        this.isAllDay,
        this.status});
}

