import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:todo_firebase/auth/authscreen.dart';

import 'dashboard/home_page.dart';
import 'listing/home.dart';
import 'profile/profile_Screen.dart';
import 'task/add_task.dart';
import 'task/edit_task.dart';
class RouteManger {
  static const String route_splash = "/splash";
  static const String route_login = "/login";

  static const String route_home_page = "/LoadDataFirebase";
  static const String route_add_task = "/AddTask";

  static const String route_all_task = "/Home";
  static const String route_edit_task = "/editTask";

  static const String route_profile = "/profile";

  static Route<dynamic> generateRoute(RouteSettings settings) {
    // final args = settings.arguments;
    switch (settings.name) {
      // case route_splash:
      //   return MaterialPageRoute(builder: (_) => Splash());
      case route_login:
        return MaterialPageRoute(builder: (_) => AuthScreen());
      case route_home_page:
        return MaterialPageRoute(builder: (_) => LoadDataFromFireStore());
      case route_add_task:
        return MaterialPageRoute(builder: (_) => AddTask());
      case route_all_task:
        return MaterialPageRoute(builder: (_) => Home());
      case route_edit_task:
        return MaterialPageRoute(builder: (_) => EditTask());
      case route_profile:
        return MaterialPageRoute(builder: (_) => Profile());

      default:
        return null;
    }
  }
}
