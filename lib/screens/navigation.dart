import 'package:http/http.dart' as http;

import 'package:flutter/material.dart';
import 'package:todo_firebase/screens/listing/done_task.dart';
import 'package:todo_firebase/screens/listing/home.dart';
import 'dashboard/home_page.dart';

class Navigation extends StatefulWidget {
  Navigation(this.index);
  int index;
  @override
  _NavigationState createState() => _NavigationState();
}

class _NavigationState extends State<Navigation> {
  int _index;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _index = widget.index;
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: _index,
      length: 2,
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: WillPopScope(
          onWillPop: () => Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (context) => LoadDataFromFireStore())),
          child: Scaffold(
            appBar: AppBar(
              centerTitle: true,
              leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => LoadDataFromFireStore()));
                },
              ),
              title: Text("All Task"),
              bottom: TabBar(
                indicatorColor: Colors.white,
                tabs: [
                  Tab(
                    icon: Icon(Icons.home),
                    text: 'Pending Task',
                  ),
                  Tab(
                    icon: Icon(Icons.check),
                    text: 'Done',
                  ),
                ],
              ),
            ),
            body: TabBarView(
              children: [
                Home(),
                Done(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
