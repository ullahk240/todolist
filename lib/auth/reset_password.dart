import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:todo_firebase/auth/authForm.dart';
import 'package:validators/validators.dart';

class ResetPassword extends StatefulWidget {

  @override
  _ResetPasswordState createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> {
  String _email;
  final auth = FirebaseAuth.instance;
  @override
  Widget build(BuildContext context) {


    return Scaffold(
        // appBar: AppBar(
        // title: Text("Reset Password"),
        // ),
      body: Form(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 30.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Forgot Password",
                style: TextStyle(
                  fontSize: 30.0,
                  fontWeight: FontWeight.w600,
                  color: Colors.blue[900],
                ),
              ),
              SizedBox(
                height: 8,
              ),
              Text(
                "Enter you email address, we will send you link for reset password",
                style: TextStyle(
                  fontSize: 16.0,
                  fontWeight: FontWeight.w400,
                  color: Colors.blue,
                ),
              ),
              SizedBox(
                height: 110,
              ),

              TextFormField(
              keyboardType: TextInputType.emailAddress,
                  validator: (val) => !isEmail(val) ? "Invalid Email" : null,

              decoration: InputDecoration(
                  hintText: "Enter Your Email",
                  hintStyle: GoogleFonts.roboto(),
                  prefixIcon:
                  new Icon(Icons.email),
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(9.0),
                    borderSide: new BorderSide(),
                  ),
                  focusedBorder: OutlineInputBorder(
                      borderSide:
                      BorderSide(color: Colors.blueAccent),
                      borderRadius: BorderRadius.circular(9.0))),

                  onChanged: (value) {
                setState(() {
                  _email = value;

        });
        }

            //hintText: "Enter User Name",
          ),

              Container(
                margin: EdgeInsets.only(top: 10),
                padding: EdgeInsets.all(5),
                width: double.infinity,
                height: 70,
                child: RaisedButton(
                  color: Colors.blueAccent,
                  child:
                      Text(
                    'Reset Password',
                    style: GoogleFonts.roboto(fontSize: 15),
                  ),
                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(9),
                  ),
                  onPressed: () {
                    auth.sendPasswordResetEmail(email: _email);
                    final snackBar = SnackBar(
                      content: Text('Reset Password Successfully! check your inbox'),
                      duration: Duration(seconds: 2),
                    );
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);

                  },
                ),
              ),
              TextButton(onPressed: () {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => AuthForm()));

              },
                  child: Text(
                    'Back to Login',
                    style: GoogleFonts.roboto(
                      fontSize: 12, color: Colors.black
                    ),
                  ),
              ),
              SizedBox(width: 10.0),
            ],
          ),
        ),
      ),
    );
  }
}