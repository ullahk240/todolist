import 'package:flutter/material.dart';
import 'package:todo_firebase/auth/authForm.dart';

class AuthScreen extends StatefulWidget {
  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   centerTitle: true,
      //   title: Text("ToDo"),
      // ),
      body: AuthForm(),
    );
  }
}
