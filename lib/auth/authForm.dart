import 'dart:io';
import 'package:flutter/rendering.dart';
import 'package:http/http.dart' as http;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:page_transition/page_transition.dart';
import 'package:todo_firebase/screens/listing/home.dart';
import 'package:todo_firebase/screens/widgets/email_field_widget.dart';
import 'package:todo_firebase/screens/widgets/password_field_widget.dart';
import 'package:todo_firebase/screens/widgets/username_field_widget.dart';
import 'package:validators/validators.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'reset_password.dart';

class AuthForm extends StatefulWidget {
  @override
  _AuthFormState createState() => _AuthFormState();
}

class _AuthFormState extends State<AuthForm> {
  PickedFile _imageFile;

  final _formKey = GlobalKey<FormState>();
  var _email = '';
  var _password = '';
  var _userName = '';
  bool isLogInPage = true;
  String url ;
  String imageUrl = "https://bit.ly/3iEFYdG";


  authentication() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(_email, _email);

    final validity = _formKey.currentState.validate();
    FocusScope.of(context).unfocus();

    if (validity) {
      _formKey.currentState.save();
      submitForm(_email, _password, _userName, url);
    }
  }

  final ImagePicker image = ImagePicker();
  File file;

  getImage() async {
    var img = await image.pickImage(source: ImageSource.gallery);
    setState(() {
      file = File(img.path);
    });
  }

  submitForm(String email, String password, String username, url) async {
    final auth = FirebaseAuth.instance;
    UserCredential authResult;

    try {
      if (isLogInPage) {
        authResult = await auth.signInWithEmailAndPassword(
            email: email, password: password);
      } else {
        authResult = await auth.createUserWithEmailAndPassword(
            email: email, password: password);


        String uid = authResult.user.uid;
        // String name = DateTime.now().toString();
        // var imageFile =
        //     FirebaseStorage.instance.ref().child(name).child("/.jpg");
        // UploadTask task = imageFile.putFile(file);
        // TaskSnapshot snapshot = await task;
        // setState(() async {
        //   url = await snapshot.ref.getDownloadURL();
        // });
        //
        // print(url);


        await FirebaseFirestore.instance.collection('users').doc(uid).set({
          'username': username,
          'email': email,
          'password': password,
          'imageUrl': url,
        });
      }
    } on FirebaseAuthException catch (e) {
     if (e.code == 'email-already-in-use') {
        print('The account already exists for that email.');
        showDialog(
                context: context,
                builder: (context) =>
                new CupertinoAlertDialog(
                  title: new Text("Email already Exist"),
                  content: new Text(
                      "The account already exists for that email.\nPlease try another email"
                  ),
                  actions: <Widget>[
                    CupertinoDialogAction(
                      child: Text("OK",
                      ),
                      onPressed: () => Navigator.of(context).pop(true),
                    ),
                  ],
                ));
      }
    } catch (e) {
      print(e);
    }

  }


  Future<void> resetPassword(String email) async {
    await FirebaseAuth.instance.sendPasswordResetEmail(email: email);
  }

  @override
  void initState() {
    setState(() {
      url = imageUrl;
    });
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            color: Colors.white,
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
              scrollDirection: !isLogInPage ? Axis.vertical : Axis.horizontal,
              child: Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.only(top: 40),
                child: Form(
                  key: _formKey,
                  child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Container(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(30),
                              topLeft: Radius.circular(30)),
                        ),
                        child: Padding(
                          padding: EdgeInsets.only(left: 10, right: 10),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              // if (!isLogInPage)
                              //   InkWell(
                              //       onTap: () {
                              //         getImage();
                              //       },
                              //
                              //       child: CircleAvatar(
                              //         radius: 80,
                              //         backgroundImage: file == null
                              //             ? AssetImage("assets/images/avat.png")
                              //             : FileImage(File(file.path))
                              //                 as ImageProvider,
                              //         child: Padding(
                              //           padding: EdgeInsets.only(right: 20,bottom: 20),
                              //           child: Align(
                              //               alignment: Alignment.bottomRight,
                              //               child: Icon(Icons.camera_alt_outlined,color: Colors.blue,),
                              //             ),
                              //         ),
                              //       )),
                              // Positioned(
                              //     bottom: 20,
                              //     right: 20.0,
                              //     child: InkWell(
                              //       onTap: () {
                              //         showModalBottomSheet(
                              //             context: context,
                              //             builder: (context)=>bottomSheet());
                              //       },
                              //
                              // child: Icon(
                              //   Icons.camera_alt,
                              //   color: Colors.teal,
                              //   size: 28.0,
                              //   ),
                              //      ),
                              // ),
                              if(!isLogInPage)

                                Column(
                                children: [
                                  Text(
                                    "Create Account",
                                    style: TextStyle(
                                      fontSize: 30.0,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.blue[900],
                                    ),
                                  ),

                                  Container(
                                    margin: EdgeInsets.only(top: 8),
                                    child: Text(
                                      "Sign up to continue!",
                                      style: TextStyle(
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.blue,
                                      ),
                                    ),
                                  ),

                                ],
                              ),

                              if (!isLogInPage)
                                Container(
                                  margin: EdgeInsets.only(top: 65, bottom: 5),
                                  child: userNameField("Enter Name", -_userName, onSaved)
                                ),

                              if(isLogInPage)
                              Column(
                                children: [
                                  Text(
                                    "Welcome Back",
                                    style: TextStyle(
                                      fontSize: 30.0,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.blue[900],
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 5),
                                    child: Text(
                                      "Login to continue",
                                      style: TextStyle(
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.blue,
                                      ),
                                    ),
                                  ),

                                ],
                              ),
                              SizedBox(
                                height: 15,
                              ),
                              if(isLogInPage)
                              SizedBox(
                                height: 70,
                              ),
                              emailField("Email", _email, onSaved),
                              // labelText: "Enter Email",
                              // labelStyle: GoogleFonts.roboto()),
                              Container(
                                margin: EdgeInsets.only(bottom: 17, top: 20),
                                child: passwordField("Enter Password", _password, onSaved),
                              ),
                              // labelText: "Enter Password",
                              // labelStyle: GoogleFonts.roboto()
                              if(isLogInPage)
                                Align(
                                alignment: Alignment.topRight,
                                child: TextButton(
                                    onPressed: () {
                                      Navigator.of(context).push(
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  ResetPassword()));
                                    },
                                    child: Text(
                                      'Forgot Password?',
                                      style: GoogleFonts.roboto(
                                          fontSize: 15,
                                          fontWeight: FontWeight.w600,
                                          color: Colors.blue),
                                    ))
                              ),
                              Container(
                                padding: EdgeInsets.all(5),
                                width: double.infinity,
                                height: 70,
                                child: RaisedButton(
                                  color: Colors.blueAccent,
                                  child: isLogInPage
                                      ? Text(
                                          'Login',
                                          style: GoogleFonts.roboto(fontSize: 15),
                                        )
                                      : Text('SignUp',
                                          style:
                                              GoogleFonts.roboto(fontSize: 15)),
                                  textColor: Colors.white,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(9),
                                  ),
                                  onPressed: () {
                                    authentication();
                                  },
                                ),
                              ),
                              Container(

                                child: TextButton(
                                  onPressed: () {
                                    setState(() {
                                      isLogInPage = !isLogInPage;
                                    });
                                  },
                                  child: !isLogInPage
                                      ? Text(
                                          "Already account exist?",
                                          style: GoogleFonts.roboto(
                                              fontSize: 12,
                                              color: Colors.black38),
                                        )
                                      : Text(
                                          "Don't have any account?",
                                          style: GoogleFonts.roboto(
                                              fontSize: 12,
                                              color: Colors.black38),
                                        ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            )));
  }

  // Widget bottomSheet() {
  //   return Container(
  //     height: 100.0,
  //     width: 200.0,
  //     margin: EdgeInsets.symmetric(
  //       horizontal: 20.0,
  //       vertical: 20.0,
  //     ),
  //     child: Column(
  //       children: [
  //         Text(
  //           "Choose Profile photo",
  //           style: TextStyle(
  //             fontSize: 20.0,
  //           ),
  //         ),
  //         SizedBox(
  //           height: 20,
  //           width: 50,
  //         ),
  //         Row(mainAxisAlignment: MainAxisAlignment.center, children: [
  //           FlatButton.icon(
  //               label: Text("Camera"),
  //               icon: Icon(Icons.camera),
  //               onPressed: () {
  //                 takePhoto(ImageSource.camera);
  //               }),
  //           FlatButton.icon(
  //             icon: Icon(Icons.image),
  //             onPressed: () {
  //               takePhoto(ImageSource.gallery);
  //             },
  //             label: Text("Gallery"),
  //           )
  //         ]),
  //       ],
  //     ),
  //   );
  // }

  // void takePhoto(ImageSource source) async {
  //   final pickedFile = await _picker.getImage(
  //     source: source,
  //   );
  //   setState(() {
  //     _imageFile = pickedFile;
  //   });
  // }
}
